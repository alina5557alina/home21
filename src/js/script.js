let activeButton = null;

let event = document.addEventListener('keydown', (event) => {
  const key = event.key;
  let button = null;

  let allButtons = document.querySelectorAll('.btn');
  allButtons.forEach(function(btn) {
    if (btn.textContent.toLowerCase() === key.toLowerCase()) {
        button = btn; 
    }
  });

  if (activeButton) {
    activeButton.classList.remove('active');
  }
  if (button) {
    button.classList.add('active');
    activeButton = button; 
  } else {
    activeButton = null; 
  }
  
});

